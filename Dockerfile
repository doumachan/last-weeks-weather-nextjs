FROM mhart/alpine-node:8.9.0
# FROM doumachan/lastweeksweathernext_web:1
# FROM doumachan/lastweeksweathernext_web:2

RUN mkdir -p /src

WORKDIR /src

COPY package.json .

RUN npm i

COPY . .

EXPOSE 3000

CMD ["npm", "start"]
