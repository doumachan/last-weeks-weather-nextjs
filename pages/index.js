import React from 'react'
import Router from 'next/router'
import Head from 'next/head'

import axios from 'axios'
import moment from 'moment'
import path from 'path'

import 'bulma/css/bulma.css'

import Form from '../components/form'
import Notice from '../components/notice'
import Results from '../components/results'

const __basename = `${path.basename(__filename)}`

export default class extends React.Component {

  static async getInitialProps (props) {
    const { query } = props
    const state = {}
    Object.keys(query).forEach((key, index) => {
      state[key] = query[key]
    })
    const getPastWeek = () => {
      const pastWeek = {};
      let day = moment().set({ hour: 0, minute: 0, second: 0, millisecond: 0 }).subtract(7,'d');
      var i = 0;
      while (i < 7) {
      	pastWeek[day.unix()] = false;
        day = day.add(1, 'd');
        i++;
      }
      return pastWeek
    }
    const getRequest = async (longitude, latitude, time) => {
      const url = `https://api.darksky.net/forecast/81b1b279180f34940706387281d9bba9/${latitude},${longitude},${time}?exclude=currently,minutely,hourly,alerts,flags`
      const config = {}
      const response = await axios.get(`${url}`, config)
      const data = await response.data
      return data
    }
    const getRequests = (longitude, latitude, pastWeek) => {
      const requests = []
      Object.keys(pastWeek).forEach((day) => {
        requests.push(getRequest(longitude, latitude, day))
      })
      return requests
    }
    const hasCoordinates = ((params) => {
      const fields = ['longitude', 'latitude']
      const validated = fields.filter((field, index) => {
        if (params[field]) {
          return true
        }
        return false
      })
      if (validated.length == fields.length) {
        return true
      }
      return false
    })(query)
    if (hasCoordinates) {
      const data = await (async (longitude, latitude) => {
        const pastWeek = getPastWeek()
        const requests = getRequests(query.longitude, query.latitude, pastWeek)
        const results = await Promise.all(requests)
        return results
      })(query.longitude, query.latitude)
      state.data = data
    } else {
      state.longitude = `-117.82650490000003`
      state.latitude = `33.6845673`
    }
    return state
  }

  constructor (props) {
    const { query } = props
    super(props)
    const fields = ['longitude', 'latitude']
    const validated = fields.filter((field, index) => {
      if (props[field]) {
        return true
      }
      return false
    })
    if (validated.length == 2) {
      const state = {
        data: []
      }
      fields.forEach((field) => {
        state[field] = props[field]
      })
      if (props.data) {
        state.data = props.data
      }
      state.hasFetchedCoordinates = false
      this.state = state
    } else {
      this.state = { longitude: '', latitude: '', data: [], hasFetchedCoordinates: false }
    }
    this.getCoordinates = this.getCoordinates.bind(this)
    this.unsetHasFetchedCoordinates = this.unsetHasFetchedCoordinates.bind(this)
  }

  getCoordinates () {
    if ("geolocation" in navigator) {
      // 30 seconds
      const timeoutVal = 30000
      navigator.geolocation.getCurrentPosition((position) => {
        const state = {
          longitude: position.coords.longitude,
          latitude: position.coords.latitude,
          hasFetchedCoordinates: true
        }
        this.setState(state)
      }, (error) => {
        const errors = {
          1: 'Permission denied',
          2: 'Position unavailable',
          3: 'Request timeout'
        }
        alert(`Error encountered: ${errors[error.code]}`)
      }, { enableHighAccuracy: true, timeout: timeoutVal, maximumAge: 0 })
    } else {
      alert(`Looks like coordinates can't be fetched, sorry about that.`)
    }
  }

  unsetHasFetchedCoordinates () {
    const state = {
      hasFetchedCoordinates: false
    }
    this.setState(state)
  }

  render () {
    const title = `Last Week's Weather`
    const subtitle = `Enter longitude and latitude coordinates below, then click button.`
    return (
      <div>
        {
          <Head>
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <link rel='stylesheet' href='/_next/static/style.css' />
            <script defer src="https://use.fontawesome.com/releases/v5.0.10/js/all.js" integrity="sha384-slN8GvtUJGnv6ca26v8EzVaR9DC58QEwsIk9q1QXdCU8Yu8ck/tL/5szYlBbqmS+" crossorigin="anonymous"></script>
          </Head>
        }
        {
          <section className="section">
            <div className="container">
              <h1 className="title">{ title }</h1>
              <p className="subtitle">{ subtitle }</p>
              <Notice
                getCoordinates={ this.getCoordinates }
              />
              <Form
                latitude={ this.state.latitude }
                longitude={ this.state.longitude }
                hasFetchedCoordinates={ this.state.hasFetchedCoordinates }
                unsetHasFetchedCoordinates={ this.unsetHasFetchedCoordinates }
              />
              <br />
              <Results latitude={ this.state.latitude } longitude={ this.state.longitude } data={ this.state.data } />
            </div>
          </section>
        }
      </div>
    )
  }
}
