import React from 'react'

import moment from 'moment'

export default class extends React.Component {

  constructor (props) {
    super(props)
    this.state = {
      longitude: props.longitude,
      latitude: props.latitude,
      data: props.data
    }
  }
  render () {
    const lastWeek = this.state.data.map((day, index) => {
      const details = day.daily.data[0]
      const date = moment.unix(details.time)
      return (
        <div className="tile is-parent">
          <div className="card">
            <div className="card-content">
              <div className="content">
                <p className="title is-hidden-tablet">{ date.format('dddd') }</p>
                <p className="title is-hidden-mobile is-hidden-tablet-only is-hidden-desktop-only">{ date.format('ddd') }</p>
                <p className="title is-size-4 is-hidden-mobile is-hidden-widescreen">{ date.format('ddd') }</p>
                <p className="subtitle is-hidden-tablet-only is-hidden-desktop-only">{ date.format('MMMM Do, YYYY') }</p>
                <p className="subtitle is-size-6 is-hidden-mobile is-hidden-widescreen">{ date.format('MMMM Do, YYYY') }</p>
                <dl className="is-hidden-mobile is-hidden-fullhd">
                  <dt className="is-size-7">High:</dt>
                  <dt><strong>{ details.temperatureHigh }</strong></dt>
                  <dt className="is-size-7">Low:</dt>
                  <dt><strong>{ details.temperatureLow }</strong></dt>
                  <dt className="is-size-7">Humidity:</dt>
                  <dt><strong>{ details.humidity }</strong></dt>
                </dl>
                <table className="table is-size-7 is-hoverable is-hidden-tablet-only is-hidden-desktop-only is-hidden-widescreen-only">
                  <tbody>
                    <tr>
                      <td className="no-padding-left">High:</td>
                      <td><strong>{ details.temperatureHigh }</strong></td>
                    </tr>
                    <tr>
                      <td className="no-padding-left">Low:</td>
                      <td><strong>{ details.temperatureLow }</strong></td>
                    </tr>
                    <tr>
                      <td className="no-padding-left">Humidity:</td>
                      <td><strong>{ details.humidity }</strong></td>
                    </tr>
                  </tbody>
                </table>
                <p className="summary is-hidden-tablet-only is-hidden-desktop-only">{ details.summary }</p>
                <p className="summary is-size-7 is-hidden-mobile is-hidden-widescreen">{ details.summary }</p>
              </div>
            </div>
          </div>
          <style jsx>{`
            .no-padding-left {
              padding-left: 0;
            }
          `}</style>
        </div>
      )
    })
    return (
      <div className="tile is-ancestor">
        { lastWeek }
      </div>
    )
  }
}
