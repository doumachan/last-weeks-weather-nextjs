import React from 'react'

export default class extends React.Component {

  constructor (props) {
    super(props)
    this.state = {
      longitude: props.longitude,
      longitudeHelp: '',
      latitude: props.latitude,
      latitudeHelp: '',
      hasFetchedCoordinates: props.hasFetchedCoordinates ? props.hasFetchedCoordinates : false
    }
  }

  componentDidMount (props) {
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        const state = {
          longitude: position.coords.longitude,
          latitude: position.coords.latitude
        }
        this.setState(state)
      })
    }
  }

  componentDidUpdate (props) {
    if (props.hasFetchedCoordinates) {
      if (props.unsetHasFetchedCoordinates !== undefined) {
        props.unsetHasFetchedCoordinates()
      }
    }
  }

  hasCoordinates (params) {
    const fields = ['longitude', 'latitude']
    const validated = fields.filter((field, index) => {
      if (params[field]) {
        return true
      }
      return false
    })
    if (validated.length == fields.length) {
      return true
    }
    return false
  }

  getNewCoordinates (props, state) {
    const coordinates = {}
    if (this.props.longitude != this.state.longitude) {
      coordinates.longitude = this.props.longitude
      coordinates.longitudeHelp = ''
    }
    if (this.props.latitude != this.state.latitude) {
      coordinates.latitude = this.props.latitude
      coordinates.latitudeHelp = ''
    }
    return coordinates
  }

  render () {
    if (this.hasCoordinates(this.props) && this.props.hasFetchedCoordinates) {
      // New coordinates were fetched by button CTA
      const newCoordinates = this.getNewCoordinates()
      if (Object.keys(newCoordinates).length) {
        this.setState(newCoordinates)
      }
    }

    const helpMessage = "is required."
    const helpMessageNan = "is not a valid coordinate value."
    const buttonText = "Get last week's weather!"

    const handleClick = (e) => {
      const fields = ['longitude', 'latitude']
      const validated = fields.filter((field, index) => {
        if (this.state[field] && !this.state[`${field}Help`]) {
          return true
        }
        return false
      })
      if (validated.length != 2) {
        e.preventDefault()
      }
    }

    const handleChange = (event) => {
      const state = {}
      state[event.target.name] = event.target.value
      if (!event.target.value) {
        state[`${event.target.name}Help`] = `${event.target.placeholder} ${helpMessage}`
      } else {
        if (isNaN(event.target.value)) {
          state[`${event.target.name}Help`] = `${event.target.placeholder} ${helpMessageNan}`
        } else {
          state[`${event.target.name}Help`] = ``
        }
      }
      this.setState(state)
    }

    const getCheckedClasses = (type) => {
      let classes = `icon is-small is-right`
      if (!this.state[`${type}`]) {
        classes = `${classes} is-invisible`
      } else if (this.state[`${type}Help`]) {
        classes = `${classes} is-invisible`
      }
      return classes
    }

    const getInputClasses = (value, valueHelp) => {
      let classes = `input`
      if (!value && !valueHelp) {
        classes = `${classes}`
      } else {
        if (value) {
          classes = `${classes} is-success`
        } else if (valueHelp) {
          classes = `${classes} is-danger`
        }
      }
      return classes
    }

    return (
      <form>
        <div className="field">
          <p className="control has-icons-left has-icons-right">
            <input
              name="longitude"
              className={ getInputClasses(this.state.longitude, this.state.longitudeHelp) }
              type="text"
              placeholder="Longitude"
              value={ this.state.longitude }
              onChange={ handleChange } />
            <span className="icon is-small is-left">
              <i className="fas fa-globe"></i>
            </span>
            <span className={ getCheckedClasses('longitude') }>
              <i className="fas fa-check"></i>
            </span>
            <span className="help is-danger">{ this.state.longitudeHelp }</span>
          </p>
        </div>
        <div className="field">
          <p className="control has-icons-left has-icons-right">
            <input
              name="latitude"
              className={ getInputClasses(this.state.latitude, this.state.latitudeHelp) }
              type="text"
              placeholder="Latitude"
              value={ this.state.latitude }
              onChange={ handleChange } />
            <span className="icon is-small is-left">
              <i className="fas fa-globe"></i>
            </span>
            <span className={ getCheckedClasses('latitude') }>
              <i className="fas fa-check"></i>
            </span>
            <span className="help is-danger">{ this.state.latitudeHelp }</span>
          </p>
        </div>
        <div className="field">
          <p className="control">
            <button className="button is-success" onClick={ handleClick }>
              { buttonText }
            </button>
          </p>
        </div>
      </form>
    )
  }
}
