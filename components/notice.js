import React from 'react'

export default class extends React.Component {

  constructor (props) {
    super(props)
    this.state = {
      hasGeoLocation: false
    }
  }

  componentDidMount (props) {
    const state = {
      hasGeoLocation: "geolocation" in navigator
    }
    this.setState(state)
  }

  getMessage () {
    let message = `We'll try to fetch your coordinates on page load. If it doesn't work after 15 seconds, try again: `
    if (!this.state.hasGeoLocation) {
      message = ''
    }
    return message
  }

  getLookUpLinkMessage () {
    let message = ` If you're still having trouble, look up your coordinates at: `
    if (!this.state.hasGeoLocation) {
      message = `Looks like we can't fetch your coordinates, you can go look up coordinates here: `
    }
    return message
  }

  getLookUpLinkUrl () {
    const url = `https://www.gps-coordinates.org`
    return url
  }

  getLookUpLinkMessageDefault () {
    let message = `We'll start you off with coordinates for Irvine, CA. You can look up other coordinates here: `
    return message
  }

  getButtonClasses () {
    let classes = `button is-small is-inverted`
    if (!this.state.hasGeoLocation) {
      classes = `${classes} is-hidden`
    }
    return classes
  }

  render () {
    const buttonText = `Fetch coordinates`
    const url = this.getLookUpLinkUrl()
    return (
      <div className="notification">
        <p className="is-hidden">
          <span className="is-size-7">{ this.getMessage() }</span>
          <button className={ this.getButtonClasses() } onClick={ this.props.getCoordinates }>{ buttonText }</button>
        </p>
        <p className="is-hidden">
          <span className="is-size-7">{ this.getLookUpLinkMessage() }</span>
          <a href={ url } className="is-size-7" target="_blank">{ url }</a>
        </p>
        <p>
          <span className="is-size-7">{ this.getLookUpLinkMessageDefault() }</span>
          <a href={ url } className="is-size-7" target="_blank">{ url }</a>
        </p>
      </div>
    )
  }
}
